#include "game.h"
#include "car.h"

Game::Game(QOpenGLWidget* context): context(context)
{
    initializeOpenGLFunctions();
    running = true;
    dimensions = QVector2D(context->width(), context->height());
    backgroundColor = QVector3D(141, 141, 141);

    player = new Player(this);
    player->startPosition();
    deaths = score = 0;

    stage = 0;
    stagePath = QString(":/stages/stage%1").arg(stage);

    cars = new Car*[LANES];
    for (unsigned int i = 0; i < LANES; ++i)
        cars[i] = nullptr;

    cars = StageLoader::getInstance()->load(cars, stagePath, LANES, -12, 12);

    QTimer timer;
    connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
    timer.start(1000/60);
}

void Game::loop()
{
    if (!running)
        return;

    context->makeCurrent();
    update();
    draw();
    context->update();
}

void Game::handleKey(int key)
{
    if (player) {
        if (key == Qt::Key_Up)
            player->translate(0, 1);
        else if (key == Qt::Key_Down)
            player->translate(0, -1);
        else if (key == Qt::Key_Left)
            player->translate(-1, 0);
        else if (key == Qt::Key_Right)
            player->translate(1, 0);
    }
}

void Game::roundStart()
{
    player->startPosition();
}

void Game::chickenDied()
{
    static QTimer timer;
    timer.setSingleShot(true);

    if (timer.remainingTime() <= 0) {
        roundStart();
        deaths++;
        emit died(deaths);
        timer.start(200);
    }
}

void Game::chickenScored()
{
    roundStart();
    score++;
    stage = (stage + 1) % STAGES;
    stagePath = QString(":/stages/stage%1").arg(stage);
    cars = StageLoader::getInstance()->load(cars, stagePath, LANES, -12, 12);
    emit scored(score);
}


void Game::draw()
{
    glClearColor(backgroundColor.x() / 255.0, backgroundColor.y() / 255.0, backgroundColor.z() / 255.0, 1);
    player->draw();

    for (unsigned int i = 0; i < LANES; ++i) {
        if (cars[i])
            cars[i]->draw();
    }
}

void Game::update()
{
    for (unsigned int i = 0; i < LANES; ++i) {
        if (cars[i])
            cars[i]->move();
    }

    if (player->intersecting(cars, LANES))
        chickenDied();

}

Game::~Game()
{
    delete player;
    delete[] cars;
}
