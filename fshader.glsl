#version 410

in vec4 vColor;

out vec4 vFragColor;

void main(void)
{
    vFragColor = vColor;
}
