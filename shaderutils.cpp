#include "shaderutils.h"

ShaderUtils* ShaderUtils::instance = nullptr;

ShaderUtils::ShaderUtils()
{
    initializeOpenGLFunctions();
}

ShaderUtils* ShaderUtils::getInstance()
{
    if (!instance)
        instance = new ShaderUtils;

    return instance;
}

GLuint ShaderUtils::createShader(const QString &file, GLenum type)
{
    QFile sf(file);
    sf.open(QFile::ReadOnly | QFile::Text);
    std::string contentStdString = sf.readAll().toStdString();
    const char* content = contentStdString.c_str();

    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &content, NULL);
    glCompileShader(shader);

    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled == GL_FALSE) {
        GLchar message[1024];
        glGetShaderInfoLog(shader, 1024, NULL, message);
        qInfo("Shader Compile Error: %s", message);

        return GL_FALSE;
    }

    return shader;
}

GLuint ShaderUtils::createProgram(const GLuint vshader, const GLuint fshader)
{
    GLuint program = glCreateProgram();

    glAttachShader(program, vshader);
    glAttachShader(program, fshader);
    glLinkProgram(program);

    GLint linked;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked == GL_FALSE) {
        GLchar message[1024];
        glGetProgramInfoLog(program, 1024, NULL, message);
        qInfo("Program Linking Error: %s", message);

        return GL_FALSE;
    }

    glDeleteShader(vshader);
    glDeleteShader(fshader);
    return program;
}
