#ifndef STAGELOADER_H
#define STAGELOADER_H

#include <QString>

#include "car.h"
#include "math.h"

class StageLoader
{
private:
    static StageLoader* instance;

public:
    static StageLoader* getInstance();
    Car** load(Car** cars, QString file, int lanes, int minPos, int maxPos);

private:
    StageLoader();
};

#endif // STAGELOADER_H
