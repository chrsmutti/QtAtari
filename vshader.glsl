#version 410

layout (location = 0) in vec4 vPosition;

uniform mat4 mModel;
uniform vec4 vColorIn;

out vec4 vColor;

void main()
{
    gl_Position = mModel * vPosition;
    vColor = vColorIn;
}
