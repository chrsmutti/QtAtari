#ifndef SHADERUTILS_H
#define SHADERUTILS_H

#include <QOpenGLExtraFunctions>
#include <QFile>

class ShaderUtils : protected QOpenGLExtraFunctions
{
private:
    static ShaderUtils* instance;

public:
    static ShaderUtils* getInstance();

    GLuint createShader(const QString& file, GLenum type);
    GLuint createProgram(const GLuint vshader, const GLuint fshader);

private:
    ShaderUtils();
};

#endif // SHADERUTILS_H
