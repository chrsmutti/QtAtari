#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QApplication>
#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLExtraFunctions>
#include <QKeyEvent>

#include "game.h"
#include "timeutils.h"

class OpenGLWidget : public QOpenGLWidget, protected QOpenGLExtraFunctions
{
Q_OBJECT
public:
    Game *game = nullptr;

public:
    OpenGLWidget(QWidget* parent);
    ~OpenGLWidget();

    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);
    void keyPressEvent(QKeyEvent *event);

signals:
    void keyEvent(int key);
    void drawCall();
    void scored(int score);
    void died(int deaths);

public slots:
    void scoreChanged(int score);
    void deathsChanged(int deaths);

};

#endif // OPENGLWIDGET_H
