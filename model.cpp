#include "model.h"

Model::Model(const char* vertexPath, const char* fragmentPath, const char* offPath, QVector4D color, float scale):
    color(color), scale(scale)
{
    initializeOpenGLFunctions();
    readOFF(QString(offPath));

    GLuint vshader = ShaderUtils::getInstance()->createShader(vertexPath, GL_VERTEX_SHADER);
    GLuint fshader = ShaderUtils::getInstance()->createShader(fragmentPath, GL_FRAGMENT_SHADER);

    program = ShaderUtils::getInstance()->createProgram(vshader, fshader);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vboPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glBufferData(GL_ARRAY_BUFFER, nVert * sizeof(QVector4D), vertices.get(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &vboIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * nFaces * sizeof(unsigned int), indices.get(), GL_STATIC_DRAW);
}

void Model::updateBoundingRect()
{
    boundingRect = QRect(position.toPoint() - QPoint(0, ySide), QSize(xSide, ySide));
}

void Model::setPosition(float x, float y)
{
    position = QVector2D(x, y);
}

void Model::translate(float x, float y)
{
    if ((position.x() + x >= 9 && x > 0) || (position.x() + y <= -9 && x < 0))
        x = 0;
    if ((position.y() + y >= 9 && y > 0) || (position.y() + y <= -9 && y < 0))
        y = 0;

    position += QVector2D(x, y);
}

void Model::draw() {
    modelMatrix.setToIdentity();
    modelMatrix.scale(invDiag);
    modelMatrix.translate(QVector3D(position, 1));
    modelMatrix.rotate(angle, 0, 0, 1);
    modelMatrix.translate(-midPoint);

    glBindVertexArray(vao);
    glUseProgram(program);

    GLuint locvColorIn = glGetUniformLocation(program, "vColorIn");
    glUniform4f(locvColorIn, color.x(), color.y(), color.z(), color.w());

    GLuint locmModel = glGetUniformLocation(program, "mModel");
    glUniformMatrix4fv(locmModel, 1, GL_FALSE, modelMatrix.data());

    glDrawElements(GL_TRIANGLES, nFaces * 3, GL_UNSIGNED_INT, 0);
}

Model::~Model()
{
    glDeleteBuffers(1, &vboIndices);
    glDeleteBuffers(1, &vboPos);
    glDeleteBuffers(1, &vboColor);
    glDeleteVertexArrays(1, &vao);

    vboIndices = 0;
    vboPos = 0;
    vboColor = 0;
    vao = 0;

    glDeleteProgram(program);
}

void Model::readOFF(QString const &path)
{
    QFile file(path);
    file.open(QFile::ReadOnly | QFile::Text);
    QByteArray byteArray = file.readAll();

    if (byteArray.isNull()) {
        qWarning("Cannot open file");
        return;
    }

    std::istringstream stream;
    stream.str(byteArray.toStdString());
    std::string line;
    stream >> line;
    stream >> nVert >> nFaces >> line;

    vertices = std::make_unique<QVector4D[]>(nVert);
    indices = std::make_unique<unsigned int[]>(nFaces * 3);

    if (nVert > 0) {
        float minLim = std::numeric_limits<float>::lowest();
        float maxLim = std::numeric_limits<float>::max();

        QVector4D max(minLim, minLim, 1, 1), min(maxLim, maxLim, 1, 1);

        float x, y, z;
        for (unsigned int i = 0; i < nVert; i++) {
           stream >> x >> y >> z;

           max.setX(std::max(x, max.x()));
           max.setY(std::max(y, max.y()));

           min.setX(std::min(x, min.x()));
           min.setY(std::min(y, min.y()));

           vertices[i] = QVector4D(x, y, 1, 1);
        }

        midPoint = ((min + max) * 0.5).toVector3D();
        invDiag = scale / (max - min).length();
        xSide = max.x() - min.x();
        ySide = max.y() - min.y();
    }

    unsigned int a, b, c;
    for (unsigned int i = 0; i < nFaces; i++) {
        stream >> line >> a >> b >> c;

        indices[i * 3 + 0] = a;
        indices[i * 3 + 1] = b;
        indices[i * 3 + 2] = c;
    }
}
