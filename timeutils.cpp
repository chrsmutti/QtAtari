#include "timeutils.h"

TimeUtils* TimeUtils::instance = nullptr;

TimeUtils::TimeUtils()
{
}

TimeUtils* TimeUtils::getInstance()
{
    if (!instance)
        instance = new TimeUtils;

    return instance;
}

void TimeUtils::updateDelta()
{
    long now = QDateTime::currentMSecsSinceEpoch();
    dt = (now - last)/60.0;
    last = now;
}
