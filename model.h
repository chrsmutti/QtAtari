#ifndef MODEL_H
#define MODEL_H

#include <QOpenGLExtraFunctions>
#include <QVector2D>
#include <QVector4D>
#include <QMatrix4x4>
#include <QRect>

#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>

#include "shaderutils.h"

class Model : protected QOpenGLExtraFunctions
{
public:
    QRect boundingRect;
    QVector2D position;

protected:
    QVector4D color;
    QVector3D midPoint;
    QMatrix4x4 modelMatrix;
    float invDiag;
    float scale;
    float angle = 0;
    float xSide, ySide;

    GLuint program;
    GLuint vao, vboIndices, vboPos, vboColor;

    unsigned int nVert, nFaces;
    std::unique_ptr<QVector4D[]> vertices;
    std::unique_ptr<unsigned int[]> indices;

public:
    Model(const char* vertexPath, const char* fragmentPath, const char* offPath, QVector4D color, float scale);
    ~Model();
    void draw();
    void setPosition(float x, float y);
    void translate(float x, float y);

private:
    void readOFF(QString const &path);

protected:
    void updateBoundingRect();

};

#endif // MODEL_H
