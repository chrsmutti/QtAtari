#include "stageloader.h"

StageLoader* StageLoader::instance = nullptr;

StageLoader::StageLoader()
{

}

Car** StageLoader::load(Car** cars, QString path, int lanes, int minPos, int maxPos)
{
    QFile file(path);
    file.open(QFile::ReadOnly | QFile::Text);
    QByteArray byteArray = file.readAll();

    if (byteArray.isNull()) {
        qWarning("Cannot open file");
        return nullptr;
    }

    std::istringstream stream;
    stream.str(byteArray.toStdString());

    int n;
    stream >> n;

    for (int i = 0; i < lanes; ++i) {
        if (cars[i]) {
            delete cars[i];
            cars[i] = nullptr;
        }
    }

    int step = (std::abs(minPos) + std::abs(maxPos)) / lanes;
    for (int i = 0; i < n; ++i) {
        int idx, r, g, b, dir;
        double speed;

        stream >> idx >> r >> g >> b >> speed >> dir;
        cars[idx] = new Car(QVector3D(r, g, b), minPos + ((idx+1) * step), speed, dir);
    }

    return cars;
}


StageLoader* StageLoader::getInstance()
{
    if (!instance)
        instance = new StageLoader;

    return instance;
}
