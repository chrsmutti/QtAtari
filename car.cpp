#include "car.h"

Car::Car(QVector3D color, float y, float speed, int dir) :
    Model(":/shaders/vshader.glsl", ":/shaders/fshader.glsl", ":/offs/car.off", QVector4D(color/255, 1), 0.2),
    speed(speed), dir(dir)
{
    posMax = 15;
    setPosition(posMax * (-dir), y);

    angle = 90;

    if (dir < 0) {
        angle += 180;
    }
}

void Car::move()
{
    translate(dir * speed * TimeUtils::getInstance()->dt, 0);
}

void Car::translate(float x, float y)
{
    if ((position.x() + x >= posMax && x > 0) || (position.x() + x <= -posMax && x < 0))
        position.setX(posMax * (-dir));

    position += QVector2D(x, y);
    updateBoundingRect();
}
