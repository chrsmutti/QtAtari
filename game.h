#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QOpenGLExtraFunctions>
#include <QOpenGLWidget>
#include <QDateTime>
#include <QVector2D>
#include <QVector3D>
#include <QTimer>

#include "player.h"
#include "timeutils.h"
#include "stageloader.h"

#define LANES 7
#define STAGES 4

class Player;

class Game : public QObject, protected QOpenGLExtraFunctions
{
Q_OBJECT
private:
    QOpenGLWidget* context;
    bool running = false;
    QVector2D dimensions;
    QVector3D backgroundColor;

    Player *player;
    Car **cars;

    int deaths, score, stage;
    QString stagePath;

public:
    Game(QOpenGLWidget* context);
    ~Game();
    void chickenDied();
    void chickenScored();

private:
    void draw();
    void roundStart();

public slots:
    void handleKey(int key);
    void loop();
    void update();

signals:
    void scored(int score);
    void died(int deaths);

};

#endif // GAME_H
